import React, { Component } from 'react';
import './App.css';
import { Connect, connect } from './overmind';

class App extends Component<Connect> {
  render() {
    const { overmind } = this.props
    return (
      <div className="App">
        <header className="App-header">
          <h4>{overmind.state.isLoadingPosts ? 'Loading...' : overmind.state.posts.length}</h4>
          <button onClick={overmind.actions.loadPosts}>Load Posts</button>
        </header>
      </div>
    );
  }
}

export default connect(App);
