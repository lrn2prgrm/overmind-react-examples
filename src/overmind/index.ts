import { Overmind, TConfig } from 'overmind'
import { TConnect, createConnect } from 'overmind-react'
import { state } from './state'
import * as effects from './effects'
import * as actions from './actions'

const config = {
  state,// The state of our app, available to connected components
  actions,// The actions, available to connected components
  effects,// Side effects, available only to actions
}

// For explicit typing check the Typescript guide
declare module 'overmind' {
  interface IConfig extends TConfig<typeof config> {}
}

const overmind = new Overmind(config)

export type Connect = TConnect<typeof config>
export const connect = createConnect(overmind)
